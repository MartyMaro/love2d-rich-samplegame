--https://gist.github.com/yuhanz/6688d474a3c391daa6d6
-- originally from http://stackoverflow.com/questions/6075262/lua-table-tostringtablename-and-table-fromstringstringtable-functions
-- modified fixed a serialization issue with invalid name. and wrap with 2 functions to serialize / deserialize
-- also modified and simplified for compression

ser = {}

local function serializeTable(val, name)
	local tmp = ""

	if name then
		if not string.match(name, "^[a-zA-z_][a-zA-Z0-9_]*$") then
			name = string.gsub(name, "'", "\\'")
			if type(name == "number") then
				name = "[" .. name .. "]"
			else
				name = "['" .. name .. "']"
			end
		end
		tmp = tmp .. name .. "="
	end

	if type(val) == "table" then
		tmp = tmp .. "{"

		for k, v in pairs(val) do
			tmp = tmp .. serializeTable(v, k) .. ","
		end

		tmp = tmp .. "}"
	elseif type(val) == "number" then
		tmp = tmp .. tostring(val)
	elseif type(val) == "string" then
		tmp = tmp .. string.format("%q", val)
	elseif type(val) == "boolean" then
		tmp = tmp .. (val and "true" or "false")
	else
		tmp = tmp .. '"[inserializeable datatype:' .. type(val) .. ']"'
	end

	return tmp
end

function ser.tableToString(table)
	return "return" .. serializeTable(table)
end

function ser.stringToTable(str)
	local f = loadstring(str)
	return f()
end

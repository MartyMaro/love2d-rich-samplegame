local r = {
  _VERSION     = "richbootcp v0.1.0",
  _DESCRIPTION = "Handles Lua stuff for RichL�VE that is usualy done in love.run(). Use this if you override love.run.",
  _URL         = "https://rebrand.ly/marty",
  _LICENSE     = [[
    MIT LICENSE
    Copyright (c) 2018 bio1712 and Martin Braun (modiX)
    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:
    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ]]
}

r.boot = function()
	if love.ads then
		love.ads.timer = 0
		love.ads.updateTime = 1
	end
end

r.update = function(dt)
	if love.ads and love.checkForAdsCallbacks and love.ads.timer and love.ads.updateTime then
		if love.ads.timer > 1 then
			love.checkForAdsCallbacks()
			love.ads.timer = 0
		end
		love.ads.timer = love.ads.timer + dt
	end
	if love.uads and love.checkForUAdsCallbacks then
		love.checkForUAdsCallbacks()
	end
end

return r
--Created by bio1712 and Martin Braun (modiX) for love2d
--love.ads / love.uads example

-- richbootcp = require "richbootcp" -- Optional: Use this when you override love.run.
m = love._os == "Android" or love._os == "iOS"
local score = 0
function love.load()
		-- richbootcp.boot() -- Optional: Use this when you override love.run.
		if m then
			love.mobsvc.test()
			love.ads.test()
			love.uads.test()
		end

    love.graphics.setBackgroundColor(0,0,0);
    posX = 0;
    posY = 0;
    print("Hello from LOVE!");
    timer = 0;
    dx = love.graphics.getWidth();
    dy = love.graphics.getHeight();
    bigFont = love.graphics.newFont(30);
    smallFont = love.graphics.newFont(12);

		if m then
			love.ads.updateTime = 1 --Default 1;

			love.mobsvc.signIn(function(playerId)
				print("MOBSVC ~ DYNAMIC: SIGNED IN - CALLBACK", playerId, love.mobsvc.isSignedIn())
				love.mobsvc.downloadSavedGamesMetadata(function(savedGamesMetadata)
					print("MOBSVC ~ DYNAMIC: DOWNLOADED SAVED GAMES METADATA - CALLBACK", savedGamesMetadata)
					local mostRecentSavedGameMetadata;
					for _, savedGameMetadata in ipairs(savedGamesMetadata) do -- find the most recent online savegame
						if not mostRecentSavedGameMetadata or savedGameMetadata.modifiedTime > mostRecentSavedGameMetadata.modifiedTime then
							mostRecentSavedGameMetadata = savedGameMetadata
						end
					end
					if mostRecentSavedGameMetadata then
						print("Loading most recent saved game " .. tostring(mostRecentSavedGameMetadata.name))
						if mostRecentSavedGameMetadata.name then
							love.mobsvc.downloadSavedGameData(mostRecentSavedGameMetadata.name, function(data)
								print("MOBSVC ~ DYNAMIC: DOWNLOADED SAVEGAME DATA - CALLBACK", data)
							end)
						end
					else
						print("No savegame available, please save first.")
					end
				end)
			end)
		end
		loaded = true
end

function love.mobileServiceSignedIn(playerId)
	print("STATIC: SIGNED IN - CALLBACK", playerId, love.mobsvc.isSignedIn())
end

function love.mobileServiceSavedGamesMetadataDownloaded(savedGamesMetadata)
	print("MOBSVC ~ STATIC: DOWNLOADED SAVED GAMES METADATA - CALLBACK", savedGamesMetadata)
	print("Details:")
	for i, savedGameMetadata in ipairs(savedGamesMetadata) do
		print(i .. " = { name = \"" .. tostring(savedGameMetadata.name) .. "\", description = \"" .. tostring(savedGameMetadata.description) .. "\", modifiedTime = " .. tostring(savedGameMetadata.modifiedTime) .. " }")
	end
end

function love.mobileServiceSavedGameMetadataDownloaded(savedGameMetadata)
	print("MOBSVC ~ STATIC: DOWNLOADED SAVED GAME METADATA - CALLBACK", savedGameMetadata)
	if savedGameMetadata then
		print("Details: { name = \"" .. tostring(savedGameMetadata.name) .. "\", description = \"" .. tostring(savedGameMetadata.description) .. "\", modifiedTime = " .. tostring(savedGameMetadata.modifiedTime) .. " }")
	else
		print("Details: nil")
	end
end

function love.mobileServiceSavedGameDataDownloaded(name, data)
	print("MOBSVC ~ STATIC: DOWNLOADED SAVED GAME DATA - CALLBACK", name, data)
	if data and data ~= "" then
		score = tonumber(data) or 0 -- we only store the score in this example, so we don't need to (de)serialize complex data
	else
		score = 0
	end
end

function love.mobileServiceSavedGameDataUploaded(name, success)
	print("MOBSVC ~ STATIC: UPLOADED SAVED GAME DATA - CALLBACK", name, success)
end

function love.mobileServiceSavedGameDeleted(name, success)
	print("MOBSVC ~ STATIC: DELETED SAVED GAME - CALLBACK", name, success)
end

function love.mobileServiceLeaderboardScoreUploaded(androidLeaderboardId, iosLeaderboardId, success)
	print("MOBSVC ~ STATIC: UPLOADED LEADERBOARD SCORE - CALLBACK", androidLeaderboardId, iosLeaderboardId, success)
end

function love.mobileServiceAchievementProgressIncremented(androidLeaderboardId, iosLeaderboardId, success, unlocked)
	print("MOBSVC ~ STATIC: INCREMENTED ACHIEVEMENT PROGRESS - CALLBACK", androidLeaderboardId, iosLeaderboardId, success, unlocked)
end

function love.unityAdsDidFinish(placementId, finishState)
	if placementId == "rewardedVideo" then
		love.window.showMessageBox("UnityAds Reward Callback", "Finish State: " .. finishState)
	end
end

function love.update(dt)
		-- richbootcp.update(dt) -- Optional: Use this when you override love.run.
    timer = timer + dt;
    if (timer > 1) then
        posX,posY = love.math.random(1,dx),love.math.random(1,dy);
        timer = 0;
		end
end

function love.draw()
    love.graphics.setFont(smallFont);
		love.graphics.print("Hello from LOVE",posX,posY);



    love.graphics.setColor(255,0,0,255);
    love.graphics.rectangle("fill",0,0,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("CreateBanner",15,50);

    love.graphics.setColor(255,255,0);
    love.graphics.rectangle("fill",150,0,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("ShowBanner",165,50);

    love.graphics.setColor(255,0,255,255);
    love.graphics.rectangle("fill",300,0,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("HideBanner",315,50);



    love.graphics.setColor(255,127,0,255);
    love.graphics.rectangle("fill",0,100,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("RequestIntersitial",15,150);

    love.graphics.setColor(0,255,0);
    love.graphics.rectangle("fill",150,100,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("isInterstitialLoaded",165,150);

    love.graphics.setColor(127,127,255,255);
    love.graphics.rectangle("fill",300,100,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("showInterstitial",315,150);



    love.graphics.setColor(255,127,255,255);
    love.graphics.rectangle("fill",0,200,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("RequestRewardedAd",15,250);

    love.graphics.setColor(0,255,255);
    love.graphics.rectangle("fill",150,200,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("isRewardedAdLoaded",165,250);

    love.graphics.setColor(255,0,127,255);
    love.graphics.rectangle("fill",300,200,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("showRewardedAd",315,250);



    love.graphics.setColor(255,255,127,255);
    love.graphics.rectangle("fill",0,300,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("U:isReady 'video'",15,350);

    love.graphics.setColor(127,255,255);
    love.graphics.rectangle("fill",150,300,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("U:show 'video'",165,350);

    love.graphics.setColor(255,127,255,255);
    love.graphics.rectangle("fill",300,300,150,100);
    love.graphics.setColor(0,0,255,255);
		love.graphics.print("U:show 'rewardedVideo'",315,350);



    love.graphics.setColor(255,0,0,255);
    love.graphics.rectangle("fill",0,450,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("Score += 1",15,500);

    love.graphics.setColor(255,255,0);
    love.graphics.rectangle("fill",150,450,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("ShowLeaderboard",165,500);

    love.graphics.setColor(255,0,255,255);
    love.graphics.rectangle("fill",300,450,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("UploadScore",315,500);



    love.graphics.setColor(255,127,0,255);
    love.graphics.rectangle("fill",0,550,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("ShowAchievements",15,600);

    love.graphics.setColor(0,255,0);
    love.graphics.rectangle("fill",150,550,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("UnlockAchievement",165,600);

    love.graphics.setColor(127,127,255,255);
    love.graphics.rectangle("fill",300,550,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("IncrementAchievement",315,600);



    love.graphics.setColor(255,127,255,255);
    love.graphics.rectangle("fill",0,650,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("SG: Dl Meta",15,700);

    love.graphics.setColor(0,255,255);
    love.graphics.rectangle("fill",150,650,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("SG: Dl Score (SG1)",165,700);

    love.graphics.setColor(255,0,127,255);
    love.graphics.rectangle("fill",300,650,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("SG: Dl Score (SG2)",315,700);



    love.graphics.setColor(255,255,127,255);
    love.graphics.rectangle("fill",0,750,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("SG: Del ALL",15,800);

    love.graphics.setColor(127,255,255);
    love.graphics.rectangle("fill",150,750,150,100);
    love.graphics.setColor(0,0,255,255);
    love.graphics.print("SG: Upl Score (SG1)",165,800);

    love.graphics.setColor(255,127,255,255);
    love.graphics.rectangle("fill",300,750,150,100);
    love.graphics.setColor(0,0,255,255);
		love.graphics.print("SG: Upl Score (SG2)",315,800);



    love.graphics.setFont(bigFont);
    love.graphics.setColor(255,255,255,255);
    love.graphics.print("Score: " .. score,20,415);

end

function checkButton(x1,y1,x2,y2,x,y)
    return x > x1 and x < x2 and y > y1 and y < y2;
end

function love.touchpressed(id,x,y,b)
	print("touchpressed " .. x .. " " .. y);
	if checkButton(0,0,150,100,x,y) then
		print("CreateBanner from LOVE");
		love.ads.createBanner("ca-app-pub-3940256099942544/6300978111","bottom");
	elseif checkButton(150,0,300,100,x,y) then
		print("ShowBanner from LOVE");
		love.ads.showBanner();
	elseif checkButton(300,0,450,100,x,y) then
		print("HideBanner from LOVE");
		love.ads.hideBanner();

	elseif checkButton(0,100,150,200,x,y) then
		print("RequestInterstitial from LOVE");
		love.ads.requestInterstitial("ca-app-pub-3940256099942544/1033173712");
	elseif checkButton(150,100,300,200,x,y) then
		print("isInterstitialLoaded from LOVE: " .. tostring(love.ads.isInterstitialLoaded()));
	elseif checkButton(300,100,450,200,x,y) then
		print("showInterstitial from LOVE");
		love.ads.showInterstitial();

	elseif checkButton(0,200,150,300,x,y) then
		print("RequestRewardedAd from LOVE");
		love.ads.requestRewardedAd("ca-app-pub-3940256099942544/1712485313");
	elseif checkButton(150,200,300,300,x,y) then
		print("isRewardedAdLoaded from LOVE: " .. tostring(love.ads.isRewardedAdLoaded()));
	elseif checkButton(300,200,450,300,x,y) then
		print("showRewardedAd from LOVE");
		love.ads.showRewardedAd();

	elseif checkButton(0,300,150,400,x,y) then
		print("U.isReady('video')", tostring(love.uads.isReady("video")))
	elseif checkButton(150,300,300,400,x,y) then
		print("U.show('video')")
		love.uads.show("video")
	elseif checkButton(300,300,450,400,x,y) then
		print("U.show('rewardedVideo')")
		love.uads.show("rewardedVideo")
	end

	if checkButton(0,450,150,550,x,y) then
		print("Score +1 from LOVE (no API call)");
		score = score + 1
	elseif checkButton(150,450,300,550,x,y) then
		print("ShowLeaderboard(s) from LOVE");
		love.mobsvc.showLeaderboards()
		-- love.mobsvc.showLeaderboard("CgkI0e6yzpUCEAIQAw", "Highscore")
	elseif checkButton(300,450,450,550,x,y) then
		print("UploadLeaderboardScore from LOVE");
		love.mobsvc.uploadLeaderboardScore("CgkI0e6yzpUCEAIQAw", "Highscore", score, "none", function(success)
			print("MOBSVC ~ DYNAMIC: UPLOADED LEADERBOARD SCORE - CALLBACK", success)
		end)

	elseif checkButton(0,550,150,650,x,y) then
		print("ShowAchievements from LOVE");
		love.mobsvc.showAchievements()
	elseif checkButton(150,550,300,650,x,y) then
		print("UnlockAchievement from LOVE");
		love.mobsvc.incrementAchievementProgress("CgkI0e6yzpUCEAIQBQ", "PressAButton", 1, 1, function(success, unlocked)
			print("MOBSVC ~ DYNAMIC: INCREMENTED ACHIEVEMENT PROGRESS - CALLBACK", success, unlocked)
		end)
	elseif checkButton(300,550,450,650,x,y) then
		print("IncrementAchievement from LOVE");
		love.mobsvc.incrementAchievementProgress("CgkI0e6yzpUCEAIQBA", "PressAButton10Times", 1, 10, function(success, unlocked)
			print("MOBSVC ~ DYNAMIC: INCREMENTED ACHIEVEMENT PROGRESS", success, unlocked)
		end)

	elseif checkButton(0,650,150,750,x,y) then
		print("mobsvc.downloadSavedGamesMetadata from LOVE");
		love.mobsvc.downloadSavedGamesMetadata(function(savedGamesMetadata)
			print("MOBSVC ~ DYNAMIC: DOWNLOADED SAVED GAMES METADATA - CALLBACK", savedGamesMetadata)
			print("mobsvc.downloadSavedGameMetadata('SG1') from LOVE");
			love.mobsvc.downloadSavedGameMetadata("SG1", function(savedGameMetadata)
				print("MOBSVC ~ DYNAMIC: DOWNLOADED SAVED GAMES METADATA - CALLBACK", savedGameMetadata)
			end)
		end)
		print("DownloadSavedGameMetadata 'SG1' from LOVE");
		love.mobsvc.downloadSavedGameMetadata("SG1", function(savedGameMetadata)
			print("MOBSVC ~ DYNAMIC: DOWNLOADED SAVED GAME METADATA - CALLBACK", savedGameMetadata)
		end)
	elseif checkButton(150,650,300,750,x,y) then
		print("mobsvc.downloadSavedGameData('SG1') from LOVE");
		love.mobsvc.downloadSavedGameData("SG1", function(data)
			print("MOBSVC ~ DYNAMIC: DOWNLOADED SAVEGAME DATA - CALLBACK", data)
		end)
	elseif checkButton(300,650,450,750,x,y) then
		print("mobsvc.downloadSavedGameData('SG2') from LOVE");
		love.mobsvc.downloadSavedGameData("SG2", function(data)
			print("MOBSVC ~ DYNAMIC: DOWNLOADED SAVEGAME DATA - CALLBACK", data)
		end)

	elseif checkButton(0,750,150,850,x,y) then
		print("mobsvc.deleteSavedGame('SG1') from LOVE");
		love.mobsvc.deleteSavedGame("SG1", function(success)
			print("MOBSVC ~ DYNAMIC: DELETED SAVEGAME - CALLBACK", "SG1", success)
			print("mobsvc.deleteSavedGame('SG2') from LOVE");
			love.mobsvc.deleteSavedGame("SG2", function(success2) -- put it into the callback, never call the same function twice, parallely
				print("MOBSVC ~ DYNAMIC: DELETED SAVEGAME - CALLBACK", "SG2", success2)
			end)
		end)
	elseif checkButton(150,750,300,850,x,y) then
		print("mobsvc.uploadSavedGameData('SG1') from LOVE");
		love.mobsvc.uploadSavedGameData("SG1", tostring(score), "Any description of SG1 " .. tostring(os.time()), function(success)
			print("MOBSVC ~ DYNAMIC: UPLOADED SAVEGAME DATA - CALLBACK", "SG1", success)
		end)
	elseif checkButton(300,750,450,850,x,y) then
		print("mobsvc.uploadSavedGameData('SG2') from LOVE");
		love.mobsvc.uploadSavedGameData("SG2", tostring(score), "Any description of SG2 " .. tostring(os.time()), function(success)
			print("MOBSVC ~ DYNAMIC: UPLOADED SAVEGAME DATA - CALLBACK", "SG2", success)
		end)
	end
end

function love.focus(f)
	if loaded and f and love.mobsvc then
		if not love.mobsvc.isSignedIn() then
			love.mobsvc.signIn(); -- resign in when coming back
		end
	end
end

--Callbacks

function love.interstitialFailedToLoad()
    print("love.interstitialFailedToLoad");
end


function love.interstitialClosed()
    print("love.interstitialClosed");
end


function love.rewardedAdFailedToLoad()
    print("love.rewardedAdFailedToLoad");
end



function love.rewardUserWithReward(rewardType,rewardQuantity)
    print("love.rewardUserWithReward: " .. tostring(rewardType) .. " " .. tostring(rewardQuantity));
end


function love.rewardedAdDidStop()
    print("love.rewardedAdDidStop");
end
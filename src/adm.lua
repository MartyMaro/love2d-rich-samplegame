local adm = {
  _VERSION     = "adm v0.1.1",
  _DESCRIPTION = "Optional AdMob wrapper for admob ports (requires beholder)",
  _URL         = "https://rebrand.ly/marty",
  _LICENSE     = [[
    MIT LICENSE
    Copyright (c) 2018 Martin Braun
    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:
    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ]]
}

function adm.showBanner() if not love.ads or not adm.initialized then return nil end
	if not adm.bannerIsVisible then
		love.ads.showBanner()
		adm.bannerIsVisible = true
	end
end

function adm.hideBanner() if not love.ads or not adm.initialized then return nil end
	if adm.bannerIsVisible then
		love.ads.hideBanner()
		adm.bannerIsVisible = false
	end
end

function adm.tryShowInterstitialAd(resultWhenNoSupport, onSuccess, onFail)
	love.interstitialClosed = function()
		if love.ads then love.ads.requestInterstitial(adm.interstitialId) end
		if onSuccess then onSuccess() end
	end
	love.interstitialFailedToLoad = onFail
	if adm.initialized then
		if love.ads then
			if love.ads.isInterstitialLoaded() then
				love.ads.showInterstitial()
				return true
			end
		else
			if resultWhenNoSupport then
				love.interstitialClosed()
				return true
			else
				love.interstitialFailedToLoad()
			end
		end
	end
	return false
end

function adm.requestRewardedAd(id)
	if love.ads then
		love.ads.requestRewardedAd(id)
	end
end

function adm.tryShowRewardedAd(resultWhenNoSupport, onSuccess, onFail)
	love.rewardedAdDidStop = onSuccess or function() return end
	love.rewardedAdFailedToLoad = onFail or function() return end
	if adm.initialized then
		if love.ads then
			if love.ads.isRewardedAdLoaded() then
				love.ads.showRewardedAd()
				return true
			end
		else
			if resultWhenNoSupport then
				love.rewardedAdDidStop()
				return true
			else
				love.rewardedAdFailedToLoad()
			end
		end
	end
	return false
end

function adm.init(bannerId, bannerPos, interstitialId, beholderLib) if not love.ads then return nil end -- mobile support only
	adm.initialized = true
	adm.interstitialId = interstitialId
	adm.bannerId = bannerId
	adm.bannerPos = bannerPos or "bottom"
	local b = beholderLib or beholder
	if adm.bannerId then
		love.ads.createBanner(adm.bannerId, adm.bannerPos)
	end
	if adm.interstitialId then
		love.ads.requestInterstitial(adm.interstitialId)
	end
	if b then
		love.rewardUserWithReward = function(rewardType, rewardQuantity)
			b.trigger("ADS.REWARD", rewardType, rewardQuantity)
		end
	end
end

return adm
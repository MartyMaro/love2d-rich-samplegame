function love.conf(t)
	t.identity = "net.example.newgame"
	t.version = "0.10.2"
	t.window.width = 500
	t.window.height = 850
	t.window.highdpi = love._os == "iOS"
end
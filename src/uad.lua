local uad = {
  _VERSION     = "uad v0.1.1",
  _DESCRIPTION = "Optional UnityAds wrapper for UnityAds ports (requires beholder)",
  _URL         = "https://rebrand.ly/marty",
  _LICENSE     = [[
    MIT LICENSE
    Copyright (c) 2018 Martin Braun
    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:
    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ]]
}

function uad.tryShowAd(resultWhenNoSupport, placementId, onSuccess, onFail, rewardType, rewardQuantity)
	placementId = placementId or uad.defaultPlacementId
	love.unityAdsDidFinish = function(pId, state)
		if onSuccess then onSuccess(pId, state) end
		if rewardType and rewardQuantity and state == "Completed" then
			uad.beholder.trigger("ADS.REWARD", rewardType, rewardQuantity)
		end
	end
	love.unityAdsDidError = onFail
	if love.uads then
		if love.uads.isReady(placementId) then
			love.uads.show(placementId)
			return true
		end
	else
		if resultWhenNoSupport then
			if love.unityAdsDidFinish then 
				love.unityAdsDidFinish(placementId, "Completed")
			end
			return true
		elseif love.unityAdsDidError then
			love.unityAdsDidError("NotInitialized", "")
		end
	end
	return false
end

function uad.init(defaultPlacementId, beholderLib)
	uad.beholder = beholderLib or beholder
	if not love.uads then return nil end -- mobile support only
	uad.defaultPlacementId = defaultPlacementId
end

return uad